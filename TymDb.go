package TymDb

import (
	"gitee.com/tym_hmm/db_store/db"
	"github.com/asdine/storm/v3"
)

type DbInterface interface {
	//初始化数据库
	Initialize(name string, dir string) (*db.TymDb, *db.DbError)

	//获取数据db对象
	GetDb() *storm.DB

}

func NewDb() DbInterface {
	return &db.TymDb{}
}
