package db

import (
	"fmt"
	"gitee.com/tym_hmm/db_store/helper/pathHelper"
	"github.com/asdine/storm/v3"
	"regexp"
	"strings"
)

type TymDb struct {
	dir    string
	dbName string
	db     *storm.DB
}

/**
初始化数据据
*/
func (tdb *TymDb) Initialize(name string, dir string) (*TymDb, *DbError) {
	tdb.dir = dir
	tdb.dbName = name
	dbPath, err := tdb.getDbPath()
	db, errs := storm.Open(dbPath, storm.Batch())
	//db, errs := storm.Open(dbPath, storm.Codec(sereal.Codec))
	if errs != nil {
		return nil, NewDbError(CODE_OPEN_DB_FILE_ERROR, fmt.Sprintf("open %s error", dbPath), err)
	}
	tdb.db = db
	return tdb, nil
}

func (tdb *TymDb) GetDb() *storm.DB {
	return tdb.db
}

func (tdb *TymDb) getDbPath() (string, *DbError) {
	dbDir, err := tdb.getDbDir()
	if err != nil {
		return "", err
	}
	dbName := fmt.Sprintf("%s.db", tdb.dbName)
	dbPath := fmt.Sprintf("%s%s%s", dbDir, pathHelper.GetPathSeparator(), dbName)
	reg := regexp.MustCompile(`[/|\\]+`)
	dbPath = reg.ReplaceAllString(dbPath, pathHelper.GetPathSeparator())
	return dbPath, nil
}

func (tdb *TymDb) getDbDir() (string, *DbError) {
	currentDir, err := pathHelper.GetCurrentPath()
	if err != nil {
		return "", NewDbError(CODE_DIR_ERROR, "current dir error", err)
	}
	var dbDir string
	if len(strings.TrimSpace(tdb.dir)) == 0 || strings.TrimSpace(tdb.dir) == "." {
		dbDir = currentDir
	} else {
		dbDir = fmt.Sprintf("%s%s%s", currentDir, pathHelper.GetPathSeparator(), tdb.dir)
	}
	dbDir = fmt.Sprintf("%s%s%s%s", dbDir, pathHelper.GetPathSeparator(), "data", pathHelper.GetPathSeparator())

	_, err = pathHelper.MkDirs(dbDir)
	if err != nil {
		return "", NewDbError(CODE_CREATE_DB_DIR_ERROR, "create db dir error", err)
	}
	return dbDir, nil
}
