package pathHelper

import (
	"os"
	"strings"
)

/**
获取目录分隔
*/
func GetPathSeparator() string {
	return "/"
}

/**
aqt获b取当前项目录
*/
func GetCurrentPath() (string, error) {
	dir, err := os.Getwd()
	if err != nil {
		return "", err
	}
	return strings.Replace(dir, "\\", "/", -1), nil
}
