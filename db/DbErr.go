package db

import "fmt"

const (
	//当前目录错误
	CODE_DIR_ERROR = 50001

	//数据库打开失败
	CODE_OPEN_DB_FILE_ERROR = 50002
	//创建数据库目录失败
	CODE_CREATE_DB_DIR_ERROR = 50003
)

type DbError struct {
	code    int64
	message string
	reason  error
}

func (dbErr *DbError) Error() string {
	return fmt.Sprintf("code:%d, message:%s, reason:%s", dbErr.code, dbErr.message, dbErr.reason)
}

func NewDbError(code int64, message string, err error) *DbError {
	return &DbError{
		code:    code,
		message: message,
		reason:  err,
	}
}

func (dbErr *DbError) GetCode() int64 {
	return dbErr.code
}

func (dbErr *DbError) GetMessage() string {
	return dbErr.message
}
func (dbErr *DbError) GetReason() error {
	return dbErr.reason
}
