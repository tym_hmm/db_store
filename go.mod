module gitee.com/tym_hmm/db_store

go 1.16

require (
	github.com/asdine/storm/v3 v3.2.1 // indirect
	go.etcd.io/bbolt v1.3.6 // indirect
	golang.org/x/sys v0.0.0-20211205182925-97ca703d548d // indirect
)
